# Bit Byte Structs

A crate to provide boiler plate for creating bus agnostic drivers for peripherals with register
based configuration.

This crate helps reduce boiler plates by providing traits and structs to help libs creating peripheral libraries. These can be
bus agonistic and are currently implementing I2C and SPI traits of the embedded-hal lib.

This is very much a first cut and still has some ruff edges.

## Documentation

Currently the docs can be found at https://pointswaves.gitlab.io/bit-byte-structures/bit_byte_structs/index.html

## License

This is currently licensed under the MIT license.

## Interactions

Please feel free to create a issue to report a problem, or to discuses a new PR before spending too much time on it.