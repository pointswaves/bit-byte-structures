//! Define a common interface trait and implement so that other code can be bus agnostic
//!
//! Implementations of the trait for a variate of buses, currently I2C and a proto for SPI
//!
//! Note that this code will end up in its own crate but it is easer to develop
//! in the same crate as a peripheral for now.
use core::fmt::Debug;
use core::marker::PhantomData;
use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use embedded_hal::blocking::spi::Transfer;
use embedded_hal::digital::v2::OutputPin;

#[derive(Debug)]
pub enum InterfaceError<E> {
    Bus(E),
    Pin,
}
impl<E> From<E> for InterfaceError<E> {
    fn from(err: E) -> Self {
        InterfaceError::Bus(err)
    }
}

/// This Trait allows all the other Bus agnostic code to interface
/// with a common bus independent api
///
/// The trait is not meant to be supper preferment as any code that
/// needs to be particularly preformat probably wants to go in
/// bus specific code.
pub trait Interface {
    type Error;

    fn read_register(&mut self, register: u8, buffer: &mut [u8]) -> Result<(), Self::Error>;
    fn write_register(&mut self, register: u8, bytes: &[u8]) -> Result<(), Self::Error>;
}

/// The I2C Implementation of the Interface trait
///
/// This seems to work and has been tested a bit.
///
/// Note the lifetimes. This struct takes ownership of the mutable borrow until it is dropped out of scope
pub struct I2CPeripheral<'a, I2C, E, const ADDRESS: u8> {
    bus: &'a mut I2C,
    phantom_e: PhantomData<E>,
}

impl<'a, I2C, E, const ADDRESS: u8> I2CPeripheral<'a, I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    /// This makes a peripheral struct that implements the Interface trait
    ///
    /// The peripheral takes ownership of the mut borrow for the life
    /// Of the struct so the struct is only ever expected to live breifely
    /// but it might be nice to make a parent thing that know the chip id etc
    /// that can have long life and then spawns on of these periodically
    pub fn new(bus: &'a mut I2C) -> I2CPeripheral<'a, I2C, E, ADDRESS> {
        I2CPeripheral {
            phantom_e: PhantomData,
            bus,
        }
    }
}

impl<'a, I2C, E, const ADDRESS: u8> Interface for I2CPeripheral<'a, I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    type Error = InterfaceError<E>;

    /// Read buffer.len() bytes starting at the reg at register
    /// This is the i2c implementation for the interface trait
    fn read_register(&mut self, register: u8, buffer: &mut [u8]) -> Result<(), Self::Error> {
        self.bus.write(ADDRESS, &[register])?;
        self.bus.read(ADDRESS, buffer)?;
        Ok(())
    }

    /// Write  buffer.len() bytes starting at the reg at register
    /// This is the i2c implementation for the interface trait
    /// *NOTE*: The current implementation only works for 10 bytes
    ///         And will panic if more are given.
    fn write_register(&mut self, register: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        let mut new_buff: [u8; 10] = [0; 10];
        new_buff[0] = register;
        if bytes.len() > 10 {
            // this should fail but i havent
            // worked out a nice way yet
            panic!("cant write that bigger slice AHAH")
        }
        for iii in 1..(bytes.len() + 1) {
            new_buff[iii] = bytes[iii - 1]
        }
        self.bus.write(ADDRESS, &new_buff[0..(bytes.len() + 1)])?;
        Ok(())
    }
}

/// A struct to implement the Interface trait
///
/// This seems to work but is not extensively tested
///
/// The read and write functions also mange the chip select pin
/// meaning you do not need to
pub struct SPIPeripheral<'a, SPI, CS, E> {
    bus: &'a mut SPI,
    cs: &'a mut CS,
    phantom_e: PhantomData<E>,
}

impl<'a, SPI, CS, E> SPIPeripheral<'a, SPI, CS, E>
where
    SPI: Transfer<u8>,
    CS: OutputPin,
{
    pub fn new(bus: &'a mut SPI, cs: &'a mut CS) -> Self {
        SPIPeripheral {
            phantom_e: PhantomData,
            bus,
            cs,
        }
    }
}

impl<'a, SPI, CS, E> Interface for SPIPeripheral<'a, SPI, CS, E>
where
    SPI: Transfer<u8, Error = E>,
    CS: OutputPin,
{
    type Error = InterfaceError<E>;

    fn read_register(&mut self, register: u8, buffer: &mut [u8]) -> Result<(), Self::Error> {
        let mut local_buff: [u8; 50] = [0; 50];
        // If the first bit is 1, the register is read.
        self.cs.set_low().map_err(|_| InterfaceError::Pin)?;
        local_buff[0] = register | 0b10_00_00_00;

        let result = self.bus.transfer(&mut local_buff[0..(buffer.len() + 1)]);

        self.cs.set_high().map_err(|_| InterfaceError::Pin)?;
        // Always reset the pin before returning the error if it exists
        result?;

        for iii in 1..(buffer.len() + 1) {
            buffer[iii - 1] = local_buff[iii]
        }
        Ok(())
    }

    fn write_register(&mut self, register: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        self.cs.set_low().map_err(|_| InterfaceError::Pin)?;
        // If the first bit is 0, the register is written.
        let mut new_buff: [u8; 10] = [0; 10];
        new_buff[0] = register & 0b01_11_11_11;
        if bytes.len() > 10 {
            // this should fail but i havent
            // worked out a nice way yet
            panic!("cant write that bigger slice AHAH")
        }
        for iii in 1..(bytes.len() + 1) {
            new_buff[iii] = bytes[iii - 1]
        }
        let result = self.bus.transfer(&mut new_buff[0..(bytes.len() + 1)]);
        self.cs.set_high().map_err(|_| InterfaceError::Pin)?;
        // Always reset the pin before returning the error if it exists
        result?;
        Ok(())
    }
}
