//! # Register based structures
//!
//! This mod builds on the Interface trait defined in the bus module and defines
//! a series of structs that mirror the common register types of many peripherals.
//!
//! ## Sub byte
//!
//! Often a byte will be broken down in to bits or a couple of bits
//! and you must then write code to bit shift and merge existing config to write
//! or bit mask and bit shift to read.
//!
//! This code dose this for you, for very time critical code, bus specific
//! code may be valuable but for many registers this code can save time and
//! code lines.
//!
//! ## Multi byte
//!
//! The first two Multi byte implementations are CrossByteBitStructI16 and ByteStructI16
//!
//! The CrossByteBitStructI16 allows you to mange values that are less than or equal to 16 bits
//! in size were they value is split over 2 bytes.
//!
//! The CrossByteBitStructI16 structure allows for you to retrieve a number of I16 values in
//! adjacent registers.

use super::bus::{Interface, InterfaceError};
use core::fmt::{self, Debug};

use core::marker::PhantomData;
use core::usize;

/// A error type returned by many of the structures in this mod
///
/// This error is generic over any type of E including SPI and I2C
/// the struct also implements `from` trait for the underlying error and
/// will report BitByteStructError::Bus in this case.
///
/// TODO: better support bus errors by passing them though better, possibly by
/// implementing things more spiffily.
pub enum BitByteStructError<E> {
    TooManyBits,
    TooBigShift,
    InputToBig,
    InterfaceError(InterfaceError<E>),
    BusError(E),
}

impl<E> From<E> for BitByteStructError<E> {
    fn from(err: E) -> Self {
        BitByteStructError::BusError(err)
    }
}

impl<E> Debug for BitByteStructError<E>
//where
//    E: Debug
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            //Self::BusError(e) => f.write_fmt(format_args!("BitByteStructError bus error {:?}", e))?,
            //Self::InterfaceError(e) => f.write_fmt(format_args!("BitByteStructError interface error {:?}", e))?,
            Self::BusError(_) => f.write_str("BitByteStructError Bus error")?,
            Self::InterfaceError(_) => f.write_str("BitByteStructError Interface error")?,
            Self::TooManyBits => f.write_str("BitByteStructError too many bits error")?,
            Self::TooBigShift => f.write_str("BitByteStructError too big shift error")?,
            Self::InputToBig => f.write_str("BitByteStructError input too big error")?,
        }

        Ok(())
    }
}

/// Struct for representing registers of 0-8bits
pub struct BitStruct<Interface: ?Sized, const ADDRESS: u8, const BITS: u8, const SHIFT: u8> {
    phantom: PhantomData<Interface>,
}

impl<I, const ADDRESS: u8, const BITS: u8, const SHIFT: u8> BitStruct<I, ADDRESS, BITS, SHIFT>
where
    I: Interface,
    I: ?Sized,
{
    /// Create a new BitStruct
    ///
    /// Arguments:
    ///
    /// * `register_address: u8` - The address of the register that contains the bits.
    /// * `bits: u8` - The number of bits this struct can read and write.
    /// * `shift: u8` - The number of bits from the start of the byte.
    pub fn new() -> Result<BitStruct<I, ADDRESS, BITS, SHIFT>, BitByteStructError<I::Error>> {
        if BITS > 8 {
            return Err(BitByteStructError::TooManyBits);
        }
        if SHIFT + BITS > 8 {
            return Err(BitByteStructError::TooBigShift);
        }
        Ok(BitStruct {
            phantom: PhantomData,
        })
    }

    fn mask_size(&self) -> u8 {
        (2_u16.pow(BITS as u32) - 1) as u8
    }

    /// Read the value from the peripheral
    ///
    /// * `bus: &mut dyn Interface<Error = I::Error>` - The interface to the bus over which to communicate with the peripheral.
    ///
    /// This will read the register that contains the information and then mask and shit the value for you.
    pub fn read(
        &self,
        bus: &mut dyn Interface<Error = I::Error>,
    ) -> Result<u8, BitByteStructError<I::Error>> {
        let mut active_buffer: [u8; 1] = [0];
        bus.read_register(ADDRESS, &mut active_buffer)?;
        Ok((active_buffer[0] & (self.mask_size() << SHIFT)) >> SHIFT)
    }

    /// Write the value to the peripheral
    ///
    /// * `bus: &mut dyn Interface<Error = I::Error>` - The interface to the bus over which to communicate with the peripheral.
    /// * `new_value: u8` - The new value to send to the peripheral
    ///
    /// This will read the register that contains the information and then mask and shit the new value and then write the full
    /// register back without effecting the other bits not covered by this structures bits.
    ///
    /// If `new_value` is larger than the number of bits this structure covers then the function will return a `BitByteStructError::InputToBig`
    /// without interacting with the peripheral.
    pub fn write(
        &self,
        bus: &mut dyn Interface<Error = I::Error>,
        new_value: u8,
    ) -> Result<(), BitByteStructError<I::Error>> {
        // This should be turned off with feature flag
        let mask = self.mask_size() << SHIFT;
        if new_value != new_value & self.mask_size() {
            return Err(BitByteStructError::InputToBig);
        }
        let mut active_buffer: [u8; 1] = [0];
        bus.read_register(ADDRESS, &mut active_buffer)?;
        active_buffer[0] = (active_buffer[0] & !mask) | new_value << SHIFT;
        bus.write_register(ADDRESS, &active_buffer)?;
        Ok(())
    }
}

/// A register for interacting with 0-16bits over two consecutive bytes/registers
///
/// This reads/writes two bytes but provides a api presenting a u16 to the lib using it.
pub struct CrossByteBitStructI16<Interface: ?Sized> {
    phantom: PhantomData<Interface>,
    register_address: u8,
    bits: u8,
    /// Set true for Big endian registers
    big_endian: bool,
}

impl<I> CrossByteBitStructI16<I>
where
    I: Interface,
    I: ?Sized,
{
    pub fn new(
        register_address: u8,
        bits: u8,
        big_endian: bool,
    ) -> Result<CrossByteBitStructI16<I>, BitByteStructError<I::Error>> {
        Ok(Self {
            phantom: PhantomData,
            register_address,
            bits,
            big_endian,
        })
    }

    fn mask_size_full(&self) -> u16 {
        (2_u32.pow(self.bits as u32) - 1) as u16
    }

    fn mask_size_byte(&self) -> u8 {
        (2_u32.pow((self.bits - 8) as u32) - 1) as u8
    }

    /// Read the new value to the relevant sub set of the registers
    pub fn read(
        &self,
        bus: &mut dyn Interface<Error = I::Error>,
    ) -> Result<u16, BitByteStructError<I::Error>> {
        let mut active_buffer: [u8; 2] = [0; 2];
        bus.read_register(self.register_address, &mut active_buffer)?;
        if self.big_endian {
            Ok(u16::from_be_bytes([
                active_buffer[0] & self.mask_size_byte(),
                active_buffer[1],
            ]))
        } else {
            Ok(u16::from_le_bytes([
                active_buffer[0],
                active_buffer[1] & self.mask_size_byte(),
            ]))
        }
    }

    /// Write the new value to the relevant sub set of the registers
    pub fn write(
        &self,
        bus: &mut dyn Interface<Error = I::Error>,
        new_value: u16,
    ) -> Result<(), BitByteStructError<I::Error>> {
        // TODO: blank, and error, on too large a value
        if new_value != new_value & self.mask_size_full() {
            return Err(BitByteStructError::InputToBig);
        }
        let active_buffer: [u8; 2] = if self.big_endian {
            [((new_value & 0xFF00) >> 8) as u8, (new_value & 0xFF) as u8]
        } else {
            [(new_value & 0xFF) as u8, ((new_value & 0xFF00) >> 8) as u8]
        };
        bus.write_register(self.register_address, &active_buffer)?;
        Ok(())
    }
}

/// A Struct to map a number of 16bit integers to a number of consecutive registers
///
/// *NOTE: Currently read only for now*
///
/// Using the COUNT as part of the type should make it easier to mange the memory
/// but I haven't got it down yet. So you can currently only read for up to 10
/// integers.
pub struct ByteStructI16<Interface: ?Sized, const COUNT: usize> {
    phantom_i: PhantomData<Interface>,
    register_address: u8,
    /// Set true for Big endian registers
    big_endian: bool,
}

impl<I, const COUNT: usize> ByteStructI16<I, COUNT>
where
    I: Interface,
    I: ?Sized,
{
    pub fn new(
        register_address: u8,
        big_endian: bool,
    ) -> Result<ByteStructI16<I, COUNT>, BitByteStructError<I::Error>> {
        Ok(ByteStructI16 {
            phantom_i: PhantomData,
            register_address,
            big_endian,
        })
    }

    /// Read the values
    ///
    /// WARNING this will go horribly wrong if COUNT > 10
    pub fn read(
        &self,
        bus: &mut dyn Interface<Error = I::Error>,
    ) -> Result<[i16; COUNT], BitByteStructError<I::Error>> {
        let mut result_slice: [i16; COUNT] = [0; COUNT];
        // TODO find a way for this to be sized
        let mut raw_slice: [u8; 20] = [0; 20];

        bus.read_register(self.register_address, &mut raw_slice[0..(COUNT * 2)])?;
        for iii in 0..COUNT {
            if self.big_endian {
                result_slice[iii] =
                    i16::from_be_bytes([raw_slice[iii * 2], raw_slice[iii * 2 + 1]]);
            } else {
                result_slice[iii] =
                    i16::from_le_bytes([raw_slice[iii * 2], raw_slice[iii * 2 + 1]]);
            }
        }
        Ok(result_slice)
    }
}
